package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	api "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
)

func initMigrate(migrate *cobra.Command) {

	alloc := &cobra.Command{
		Use:   "alloc",
		Short: "migrate alloc DB to facility namspaced format",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			migrateAlloc()
		},
	}

	migrate.AddCommand(alloc)
}

func migrateAlloc() {

	prefix := "/alloc/resource/"
	kvc := clientv3.NewKV(storage.EtcdClient)
	resp, err := kvc.Get(context.TODO(), prefix, clientv3.WithPrefix())
	if err != nil {
		log.Fatalf("get key: %v", err)
		os.Exit(1)
	}

	if len(resp.Kvs) == 0 {
		log.Fatalf("No keys found in %s", prefix)
	}

	log.Infof("Reading %d resource allocations", len(resp.Kvs))

	var ifs []clientv3.Cmp
	var ops []clientv3.Op

	for _, x := range resp.Kvs {

		k := string(x.Key)

		// We are looking for keys which are not already in the facility namespaced format.
		// That format is "/alloc/resource/facilityName/.resourceName". i.e. 4 slash-separated entries.
		// So look for keys of the form "/alloc/resource/string".
		tkns := strings.Split(k, "/")
		if len(tkns) > 4 { // note strings.Split counts /a/b/c/d as 5 things split by /.
			log.Infof("Skipping migrated key %s", k)
			continue
		}

		// TODO handle case of a duplicate resource in namespaced and not format?

		l := new(api.ResourceAllocationList)
		err := proto.Unmarshal(x.Value, l)
		if err != nil {
			log.WithFields(log.Fields{
				"key": k,
			}).Warn("corrupt resource allocation encountered")
			continue
		}
		l.Revision = x.Version

		if len(l.Value) == 0 {
			log.Infof("Skipping valueless key %s", x)
			continue
		}

		resource := strings.Split(k, "/")[3]
		newKey := fmt.Sprintf("%s%s/%s", prefix, l.Value[0].Facility, resource)
		log.Infof("Migrating key %s to %s", k, newKey)

		// recreate data under new key
		ifs = append(ifs, clientv3.Compare(clientv3.Version(k), "=", x.Version))
		ops = append(ops, clientv3.OpPut(newKey, string(x.Value)))

		// delete old data
		ifs = append(ifs, clientv3.Compare(clientv3.Version(k), "=", x.Version))
		ops = append(ops, clientv3.OpDelete(k))
	}

	if len(ifs) > 0 {
		log.Infof("Running %d transactions", len(ifs))
		tnxResp, err := kvc.Txn(context.Background()).
			If(ifs...).
			Then(ops...).
			Commit()

		if err != nil {
			log.Fatalf("transaction error %s", err)
		}
		if !tnxResp.Succeeded {
			log.Fatalf("transaction failed resp %+v", tnxResp)
		}
	} else {
		log.Info("No data to migrate")
	}
}
