package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"encoding/json"

	ory "github.com/ory/kratos-client-go"
	"github.com/peterhellberg/link"
	"github.com/spf13/cobra"
	"go.uber.org/zap"
)

var (
	KratosAdminURL = "http://kratos-admin"
	cli            = KratosAdminCli()
	log            *zap.SugaredLogger
)

func init() {
	log = zap.Must(zap.NewProduction()).Sugar()

	if val, ok := os.LookupEnv("KRATOS_ADMIN_URL"); ok {
		KratosAdminURL = val
	}
}

func KratosAdminCli() *ory.APIClient {

	conf := ory.NewConfiguration()
	conf.Servers = []ory.ServerConfiguration{{
		URL: KratosAdminURL,
	}}
	return ory.NewAPIClient(conf)
}

func SDKExitOnError(err error, res *http.Response) {
	if err == nil {
		return
	}
	var body []byte
	if res != nil {
		buf, err := io.ReadAll(res.Body)
		if err != nil {
			log.Fatalf("Read response body %+v", err)
		}
		body, _ = json.MarshalIndent(buf, "", "  ")
	}
	out, _ := json.MarshalIndent(err, "", "  ")
	log.Errorf("%s\n\nAn error occurred: %+v\nbody: %s\n", out, err, body)
	os.Exit(1)
}

func GetNextPageToken(response *http.Response) (string, error) {
	links := link.ParseResponse(response)
	if nextPage, ok := links["next"]; ok {
		uri, err := url.Parse(nextPage.URI)
		if err != nil {
			return "", fmt.Errorf("failed to parse nextPage uri: %w", err)
		}

		pageToken := uri.Query().Get("page_token")
		if pageToken == "" {
			return "", fmt.Errorf("next link present, but has no page_token")
		}

		return pageToken, nil
	} else {
		// There is no next page link, so we are at the last page of the response.
		return "", nil
	}
}

func ReadIdentities() ([]ory.Identity, error) {

	pageSize := int64(250)
	var pageToken string

	allids := []ory.Identity{}

	for {
		ids, resp, err := cli.IdentityAPI.ListIdentities(context.Background()).PageSize(pageSize).PageToken(pageToken).Execute()
		SDKExitOnError(err, resp)

		allids = append(allids, ids...)

		pageToken, err = GetNextPageToken(resp)
		if err != nil {
			fmt.Printf("\nAn error occurred: %+v\n", err)
			os.Exit(1)
		}

		if pageToken == "" {
			break
		}
	}

	return allids, nil
}

func chpass(userid, password string) {

	ids, err := ReadIdentities()
	if err != nil {
		log.Fatal("read identities", zap.Error(err))
	}

	for _, id := range ids {
		traits := id.Traits.(map[string]interface{})
		if un, ok := traits["username"].(string); ok {
			if un == userid {
				body := ory.UpdateIdentityBody{
					Traits: traits,
					Credentials: &ory.IdentityWithCredentials{
						Password: &ory.IdentityWithCredentialsPassword{
							Config: &ory.IdentityWithCredentialsPasswordConfig{
								// at last...
								Password: &password,
							},
						},
					},
				}

				_, resp, err := cli.IdentityAPI.UpdateIdentity(context.Background(), id.Id).UpdateIdentityBody(body).Execute()
				SDKExitOnError(err, resp)

				fmt.Printf("password changed")
				os.Exit(0)
			}
		}
	}

	fmt.Printf("user %s not found\n", userid)
	os.Exit(1)
}

func main() {

	defer log.Sync()

	cmd := &cobra.Command{
		Use:   "portalpw [user id] [pw]",
		Short: "Change a user password in kratos",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			chpass(args[0], args[1])
		},
	}

	if _, err := cmd.ExecuteC(); err != nil {
		// log.Fatal(err)
		os.Exit(1)
	}

	os.Exit(0)
}
