package main

import (
	"context"

	log "github.com/sirupsen/logrus"
	wgd "gitlab.com/mergetb/api/wgd/v1/go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"net"
)

// grpc boilerplate
//
// Server holds grpc functions.
type Server struct{}

func Daemonize(listenon, cert, key string) error {
	log.Infof("Starting wgd. Version %s", Version)

	// creds, err := credentials.NewServerTLSFromFile(cert, key)
	// if err != nil {
	// 	log.Fatal("bad cert/key")
	// 	return fmt.Errorf("bad cert/key")
	// }

	// grpcServer := grpc.NewServer(grpc.Creds(creds))

	grpcServer := grpc.NewServer()
	wgd.RegisterWgdServer(grpcServer, &Server{})
	l, err := net.Listen("tcp", listenon)
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
		return err
	}
	log.Infof("listening on tcp://%s", listenon)
	grpcServer.Serve(l)
	return nil
}

func (s *Server) CreateContainerInterface(
	ctx context.Context, req *wgd.CreateContainerInterfaceRequest,
) (*wgd.CreateContainerInterfaceResponse, error) {

	l := log.WithFields(log.Fields{
		"enclaveid":   req.Enclaveid,
		"accessaddr":  req.Accessaddr,
		"containerid": req.Containerid,
	})
	l.Info("CreateContainerInterface")

	pubkey, err := CreateContainerInterface(
		req.Enclaveid,
		req.Accessaddr,
		req.Containerid,
		req.Peers,
	)
	if err != nil {
		l.Errorf("adding interface: %v", err)
		return nil, status.Errorf(codes.Internal, err.Error())
	}
	return &wgd.CreateContainerInterfaceResponse{
		Key: pubkey,
	}, nil
}

func (s *Server) DeleteContainerInterface(
	ctx context.Context, req *wgd.DeleteContainerInterfaceRequest,
) (*wgd.DeleteContainerInterfaceResponse, error) {

	l := log.WithFields(log.Fields{
		"containerid": req.Containerid,
	})
	l.Info("DeleteContainerInterface")

	key, err := DeleteContainerInterface(req.Containerid)
	if err != nil {
		l.Errorf("deleting interface: %v", err)
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &wgd.DeleteContainerInterfaceResponse{Key: key}, nil
}

func (s *Server) GetContainerInterface(
	ctx context.Context, req *wgd.GetContainerInterfaceRequest,
) (*wgd.GetContainerInterfaceResponse, error) {

	l := log.WithFields(log.Fields{
		"containerid": req.Containerid,
	})
	l.Info("GetContainerInterface")

	ifx, err := GetContainerInterface(req.Containerid)
	if err != nil {
		l.Errorf("get interface: %v", err)
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	if ifx == nil {
		ifx = &wgd.GetContainerInterfaceResponse{}
	}

	return ifx, nil
}

func (s *Server) AddWgPeers(
	ctx context.Context, req *wgd.AddWgPeersRequest,
) (*wgd.AddWgPeersResponse, error) {

	l := log.WithFields(log.Fields{
		"containerid": req.Containerid,
		"peers":       req.Peers,
		"exclusive":   req.Exclusive,
	})
	l.Info("AddWgPeers")

	err := AddWgPeers(req.Containerid, req.Peers, req.Exclusive)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &wgd.AddWgPeersResponse{}, nil
}

func (s *Server) DeleteWgPeers(
	ctx context.Context, req *wgd.DeleteWgPeersRequest,
) (*wgd.DeleteWgPeersResponse, error) {

	l := log.WithFields(log.Fields{
		"containerid": req.Containerid,
		"peers":       req.Peers,
	})
	l.Info("DeleteWgPeers")

	err := DeleteWgPeers(req.Containerid, req.Peers)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &wgd.DeleteWgPeersResponse{}, nil
}

func (s *Server) GetWgPeers(
	ctx context.Context, req *wgd.GetWgPeersRequest,
) (*wgd.GetWgPeersResponse, error) {

	l := log.WithFields(log.Fields{
		"containerid": req.Containerid,
	})
	l.Info("GetWgPeers")

	peers, err := GetWgPeers(req.Containerid)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &wgd.GetWgPeersResponse{
		Peers: peers,
	}, nil
}
