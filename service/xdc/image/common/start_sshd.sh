#!/bin/sh

# This scripts inserts a controlled delay before starting sshd
# to control pace of supervisord restarts

# stop system-started sshd, stubborn little bugger
/etc/init.d/ssh stop
sleep 1

/usr/sbin/sshd $@
sleep 5
exit 1
