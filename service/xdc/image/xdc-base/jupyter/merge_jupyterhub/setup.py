#!/usr/bin/env python3
# coding: utf-8
#
# ----------------------------------------------------------------------------
# Merge testbed glue for jupyterhub
# ----------------------------------------------------------------------------

import sys

from distutils.core import setup


__version__ = '0.1.1'

setup_args = dict(
    name='merge_jupyterhub',
    packages=['merge_jupyterhub'],
    version=__version__,
    description="""Merge modules for jupyterhub""",
    long_description="""A modified version of sshspawner allows to run notebooks on remote materialization nodes and uses kratos to authenticate users""",
    license="BSD",
    platforms="Linux, Mac OS X",
    keywords=['Interactive', 'Interpreter', 'Shell', 'Web'],
    classifiers=[
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    scripts=['scripts/get_port.py'],
)

# setuptools requirements
if 'setuptools' in sys.modules:
    setup_args['install_requires'] = install_requires = []
    with open('requirements.txt') as f:
        for line in f.readlines():
            req = line.strip()
            if not req or req.startswith(('-e', '#')):
                continue
            install_requires.append(req)


def main():
    setup(**setup_args)

if __name__ == '__main__':
    main()
