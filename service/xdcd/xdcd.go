package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"google.golang.org/protobuf/encoding/protojson"

	log "github.com/sirupsen/logrus"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
)

const (
	DNSMASQ_HELPER      = "/usr/local/bin/dnsmasq_helper.sh"
	ATTACHED_STATE_JSON = "/etc/mergetb/attached_state.json"
)

func ClearTunnelData() error {

	if _, err := os.Stat(DNSMASQ_HELPER); errors.Is(err, os.ErrNotExist) {
		// File does not exist, move on. This will happen on ssh-jump containers.
		return nil
	}

	log.Infof("Clearing dnsmasq data")

	domains_re := `infra\.([^ ]*)  *(\1 *)?`
	out, err := exec.Command(DNSMASQ_HELPER, "clear", domains_re).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error clearing dnsmasq configuration: %s, %s", string(out), err)
	}

	return nil
}

func ConfigureTunnel(ns, id string) error {

	if strings.Count(id, ".") != 2 {
		return fmt.Errorf("bad realization id format: %s. Should be rid.eid.pid.", id)
	}

	return updateDnsmasq(ns, id)
}

// setupDnsmasq inserts dnsmasq as an intermediary between the local resolve library and
// and the default resolver.  It makes it easy to add a special resolver for materialization
// internal domains
func setupDnsmasq() error {

	if _, err := os.Stat(DNSMASQ_HELPER); errors.Is(err, os.ErrNotExist) {
		// File does not exist, move on. This will happen on ssh-jump containers.
		log.Infof("not setting up dnsmasq, setup script isn't present")
		return nil
	}

	log.Infof("setup dnsmasq")
	out, err := exec.Command(DNSMASQ_HELPER, "setup").CombinedOutput()
	if err != nil {
		return fmt.Errorf("error updating dnsmasq - setup: %s, %s", string(out), err)
	}
	return nil
}

// updateDnsmasq configure dnsmasq to use the nameserver ip address `ns` to be used only for
// resolving the materialization domain "infra."+`rid`, as well as all of RDNS.
// Connection to that nameserver is currently tunneled and when this tunnel goes down we'll
// still be able to use the main k8s server in case we need to detach/reattach the xdc from
// inside the xdc.
// Here we also update the /etc/resolv.conf to put our materialization-internal domain first,
// so names like "a" and "b" can be resolved.
func updateDnsmasq(ns, rid string) error {

	if _, err := os.Stat(DNSMASQ_HELPER); errors.Is(err, os.ErrNotExist) {
		// File does not exist, move on. This will happen on ssh-jump containers.
		return nil
	}

	domain := "infra." + rid
	log.Infof("updating dnsmasq: nameserver: %s, domain: %s", ns, domain)
	// using shell script to do all the work
	out, err := exec.Command(DNSMASQ_HELPER, "newserver", ns, domain).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error updating dnsmasq - newserver: %s, %s", string(out), err)
	}

	return nil
}

// setXdcAttachedState saves the state of the attached mtz to ATTACHED_STATE_JSON
func setXdcAttachedState(state *xdcd.XdcAttachedState) error {
	if err := os.MkdirAll(filepath.Dir(ATTACHED_STATE_JSON), 0755); err != nil {
		return err
	}
	opts := protojson.MarshalOptions{
		Indent:          "  ",
		EmitUnpopulated: true,
		UseProtoNames:   true,
	}
	return os.WriteFile(ATTACHED_STATE_JSON, []byte(opts.Format(state)+"\n"), 0644)
}
