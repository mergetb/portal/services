package main

import (
	log "github.com/sirupsen/logrus"
)

func handleError(e error) error {
	log.Errorf("%v", e)
	return e
}
