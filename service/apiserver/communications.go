package main

import (
	"context"
	"errors"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (x *xps) EMailUsers(
	ctx context.Context, rq *portal.EMailUsersRequest,
) (*portal.EMailUsersResponse, error) {

	log.Debugf("got call to email subject %s => users %+v", rq.Subject, rq.Usernames)

	caller, err := GetEffectiveGRPCCaller(ctx)

	if err != nil {
		return nil, err
	}

	// Check policy against all users that are getting email.
	// caller must have user update permissions.
	for _, username := range rq.Usernames {
		err = policy.UpdateUser(caller, username)
		if err != nil {
			// Add detail about the error.
			var me *merror.MergeError
			if errors.As(err, &me) {
				me.Instance = username
				if errors.Is(me, merror.ErrForbidden) {
					me.Detail = fmt.Sprintf("You are not able to access user %s due to policy.", username)
				}
				return nil, merror.ToGRPCError(me)
			} else {
				// Should not happen - policy always returns merge errors.
				return nil, merror.ToGRPCError(err)
			}
		}
	}

	e := storage.NewEMailRequest(
		storage.EMAIL_USERS_TYPE,
		rq.Usernames,
		caller.Email,
		rq.Subject,
		rq.Contents,
		rq.ContentType,
	)

	err = e.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "email request read")
	}

	if !e.Exists() {
		_, err = e.Create()
		if err != nil {
			return nil, fmt.Errorf("internal error: %v", err)
		}
	}

	resp := &portal.EMailUsersResponse{}

	if rq.StatusMS != 0 {
		tf, err := e.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		resp.Status = portal.NewTaskTreeFromReconcileForest(tf)
	}

	return resp, nil
}

func (x *xps) EMailProjectMembers(
	ctx context.Context, rq *portal.EMailProjectMembersRequest,
) (*portal.EMailProjectMembersResponse, error) {

	log.Debugf("got call to email subject %s => project %s", rq.Subject, rq.Project)

	caller, err := GetEffectiveGRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	p := storage.NewProject(rq.Project)
	err = p.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "project data read")
	}

	if !p.Exists() {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("project %s does not exist", rq.Project))
	}

	// Need to be able to update the projects to email its users.
	err = policy.UpdateProject(caller, rq.Project)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	e := storage.NewEMailRequest(
		storage.EMAIL_PROJECT_TYPE,
		[]string{rq.Project},
		caller.Email,
		rq.Subject,
		rq.Contents,
		rq.ContentType,
	)

	err = e.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "email request read")
	}

	if !e.Exists() {
		_, err = e.Create()
		if err != nil {
			return nil, fmt.Errorf("internal error: %v", err)
		}
	}

	resp := &portal.EMailProjectMembersResponse{}

	if rq.StatusMS != 0 {
		tf, err := e.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		resp.Status = portal.NewTaskTreeFromReconcileForest(tf)
	}

	return resp, nil
}

func (x *xps) EMailOrganizationMembers(
	ctx context.Context, rq *portal.EMailOrganizationMembersRequest,
) (*portal.EMailOrganizationMembersResponse, error) {

	log.Debugf("got call to email subject %s => org %s", rq.Subject, rq.Organization)

	caller, err := GetEffectiveGRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	o := storage.NewOrganization(rq.Organization)
	o.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, " rganization data read")
	}

	if !o.Exists() {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("organization %s does not exist", rq.Organization))
	}

	// Need to be able to update an org to email its users.
	err = policy.UpdateOrganization(caller, rq.Organization)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	e := storage.NewEMailRequest(
		storage.EMAIL_ORGANIZATION_TYPE,
		[]string{rq.Organization},
		caller.Email,
		rq.Subject,
		rq.Contents,
		rq.ContentType,
	)

	err = e.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "email request read")
	}

	if !e.Exists() {
		_, err = e.Create()
		if err != nil {
			return nil, fmt.Errorf("internal error: %v", err)
		}
	}

	resp := &portal.EMailOrganizationMembersResponse{}

	if rq.StatusMS != 0 {
		tf, err := e.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		resp.Status = portal.NewTaskTreeFromReconcileForest(tf)
	}

	return resp, nil
}

func (x *xps) DeleteEMail(
	ctx context.Context, rq *portal.DeleteEMailRequest,
) (*portal.DeleteEMailResponse, error) {

	log.Debugf("got call to delete email %s", rq.Id)

	caller, err := GetEffectiveGRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// Load the email request to find out which policy to apply
	e := storage.NewEMailRequestById(rq.Id)
	err = e.Read()
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	if !e.Exists() {
		return nil, status.Error(codes.NotFound, "email request with that ID does not exist")
	}

	switch e.ToType {
	case storage.EMAIL_USERS_TYPE:
		for _, username := range e.To {
			err = policy.UpdateUser(caller, username)
			if err != nil {
				return nil, merror.ToGRPCError(err)
			}
		}
	case storage.EMAIL_PROJECT_TYPE:
		if err = policy.UpdateProject(caller, e.To[0]); err != nil {
			return nil, merror.ToGRPCError(err)
		}
	case storage.EMAIL_ORGANIZATION_TYPE:
		if err = policy.UpdateOrganization(caller, e.To[0]); err != nil {
			return nil, merror.ToGRPCError(err)
		}
	}

	_, err = e.Delete()
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	return &portal.DeleteEMailResponse{}, nil
}
