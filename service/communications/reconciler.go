package main

import (
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	emailBucket = storage.PrefixedBucket(&storage.EMailRequest{})
	emailKeyStr = "^" + emailBucket + "(.+)$" // could use a base64 match here
	emailKey    = regexp.MustCompile(emailKeyStr)
)

type EMailTask struct {
	Key string
}

func runReconciler() {

	log.Infof("starting communications reconciler")
	log.Infof("watching email requests at %s", emailKeyStr)

	// Things to watch:
	// * email events

	emt := storage.ReconcilerConfigCommsEmail.ToReconcilerManager(
		&EMailTask{},
	)

	emt.Run()
}

func (t *EMailTask) Parse(key string) bool {

	parts := emailKey.FindAllStringSubmatch(key, -1)

	if len(parts) > 0 {
		t.Key = key
		return true
	}

	return false
}

func (t *EMailTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Debugf("Create called on %s", t.Key)

	r := new(storage.EMailRequest)
	err := r.Unmarshal(value)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	err = sendEmail(r)

	if err != nil {
		log.Infof("error sending email: %v", err)
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (t *EMailTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Debugf("Update called on %s", t.Key)
	return nil
}

func (t *EMailTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Debugf("Ensure called on %s", t.Key)

	// Ensure for emails is a little different. If the task was successful we delete the task.
	// Otherwise attempt a resend.
	r := new(storage.EMailRequest)
	err := r.Unmarshal(value)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	if td.Status.LastStatus == reconcile.TaskStatus_Success {
		r.Delete()
		return nil
	}

	// else resend
	err = sendEmail(r)

	if err != nil {
		log.Infof("error resending email: %v", err)
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (t *EMailTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Debugf("Delete called on %s", t.Key)

	r := new(storage.EMailRequest)
	err := r.Unmarshal(value)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	log.Infof("Deleted email task for %s %s, subject: %s", r.ToType, r.To, r.Subject)

	return nil
}
