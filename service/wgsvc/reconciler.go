package main

import (
	"gitlab.com/mergetb/portal/services/pkg/storage"

	"gitlab.com/mergetb/tech/reconcile"
)

func runReconciler() {

	t1 := storage.ReconcilerConfigWireguard.ToReconcilerManager(
		&XdcTask{},
		&WgIfTask{},
		&MtzTask{},
	)

	t2 := storage.ReconcilerConfigWireguardPodwatch.ToReconcilerManager(
		&WgPodWatch{},
	)

	reconcile.RunReconcilerManagers(t1, t2)
}
