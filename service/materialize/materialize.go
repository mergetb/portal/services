package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/connect"
	"gitlab.com/mergetb/portal/services/pkg/materialize"
	"gitlab.com/mergetb/portal/services/pkg/storage"

)

func handleMaterialize(rq *portal.MaterializeRequest) error {

	log.Printf("handling materialize")

	mzid := internal.MzidToString(&internal.Mzid{
		Rid: rq.Realization,
		Eid: rq.Experiment,
		Pid: rq.Project,
	})

	// fetch realization
	res, err := storage.ReadRealizationResult(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return fmt.Errorf("[%s] read realization result: %v", mzid, err)
	}

	if res.Diagnostics.Error() {
		return fmt.Errorf("[%s] cannot materialize failed realization", mzid)
	}

	if !res.Realization.Complete {
		return fmt.Errorf("[%s] cannot materialize incomplete realization", mzid)
	}

	// derive materialization
	mzc := &materialize.MzContext{
		GlobalUsers: []*portal.UserInfo{},
		NodeUsers:   make(map[string][]*portal.UserInfo),
	}

	mtz, err := materialize.RzToMz(res.Realization, mzc)
	if err != nil {
		return fmt.Errorf(
			"[%s] create materialization from realization: %v", mzid, err)
	}

	// save materialization
	err = storage.SaveMaterialization(mtz)
	if err != nil {
		return fmt.Errorf("[%s] save materialization: %v", mzid, err)
	}

	sites := materialize.SiteList(mtz)

	//TODO no need to send whole model to all sites, break up model into
	//fragments and only send each site what it actually needs
	for _, site := range sites {

		err = connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {
				_, err = cli.Materialize(
					context.TODO(),
					&facility.MaterializeRequest{
						Materialization: mtz,
					},
				)
				return err
			},
		)
		if err != nil {
			return fmt.Errorf("[%s] facility connection: %v", mzid, err)
		}

		log.Infof("[%s] materialization requested @ %s", mzid, site)

	}

	return nil

}

func handleDematerialize(rq *portal.MaterializeRequest) error {

	log.Printf("handling dematerialize")

	mzid := internal.MzidToString(&internal.Mzid{
		Rid: rq.Realization,
		Eid: rq.Experiment,
		Pid: rq.Project,
	})

	mtz, err := storage.ReadMaterialization(
		rq.Project,
		rq.Experiment,
		rq.Realization,
	)
	if err != nil {
		return fmt.Errorf("[%s] read materialization: %v", mzid, err)
	}

	sites := materialize.SiteList(mtz)

	for _, site := range sites {

		err = connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {
				_, err = cli.Dematerialize(
					context.TODO(),
					&facility.DematerializeRequest{
						Pid: rq.Project,
						Rid: rq.Realization,
						Eid: rq.Experiment,
					},
				)
				return err
			},
		)
		if err != nil {
			return fmt.Errorf("[%s] dematerialize: %v", mzid, err)
		}

		// TODO should wait for demat clear before removing completely
		err = storage.DeleteMaterialization(rq.Project, rq.Experiment, rq.Realization)
		if err != nil {
			return fmt.Errorf("[%s] delete materialization object: %v", mzid, err)
		}

		log.Infof("[%s] dematerialization requested @ %s", mzid, site)

	}

	return nil

}
