package main

import (
	"os"
	"path"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	xdcBucket = storage.PrefixedBucket(&storage.XDC{})
	xdcKey    = regexp.MustCompile(`^` + xdcBucket + nameExp + `\.` + nameExp + `$`)
)

type XdcTask struct {
	XDC     string
	Project string
	Key     string
}

func xId(x *portal.XDCStorage) string {
	return storage.XdcId(x.Name, x.Project)
}

func (xt *XdcTask) Parse(k string) bool {

	tkns := xdcKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		xt.XDC = tkns[0][1]
		xt.Project = tkns[0][2]
		xt.Key = k
		return true
	}

	return false
}

func (xt *XdcTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Debugf("handling xdc mergefs put")

	x := new(portal.XDCStorage)
	err := proto.Unmarshal(value, x)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	// create the xdc dir in mergefs
	err = mkDir(path.Join(xdcRoot, xId(x)), 0, 0, 0770)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	// dir for credentials
	err = mkDir(path.Join(xdcRoot, xId(x), "auth"), 0, 0, 0770)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (xt *XdcTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return xt.Create(value, version, td)
}

func (xt *XdcTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return xt.Create(value, version, td)
}

func (xt *XdcTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Debugf("handling xdc mergefs delete")

	x := new(portal.XDCStorage)
	err := proto.Unmarshal(value, x)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	// This could take a long time. TODO: get key lease.
	err = os.RemoveAll(path.Join(xdcRoot, xId(x)))
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}
