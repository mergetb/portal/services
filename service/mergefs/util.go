package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func touchFile(user, path string, mode uint32) error {

	if _, err := os.Stat(path); err == nil {
		// File exists. We're done.
		return nil
	}

	u := storage.NewUser(user)
	err := u.Read()
	if err != nil {
		return err
	}

	fd, err := os.Create(path)
	if err != nil {
		return err
	}
	// defer is LIFO
	defer fd.Close()
	defer fd.Sync()

	err = os.Chown(path, int(u.Uid), int(u.Gid))
	if err != nil {
		return fmt.Errorf("chown: %w", err)
	}

	err = os.Chmod(path, os.FileMode(mode))
	if err != nil {
		return fmt.Errorf("chmod: %w", err)
	}

	return nil
}

func lineExists(path, line string) (bool, error) {

	fd, err := os.Open(path)
	if err != nil {
		return false, err
	}
	// defer is LIFO
	defer fd.Close()
	defer fd.Sync()

	trimmed := strings.TrimSpace(line)

	scan := bufio.NewScanner(fd)
	for scan.Scan() {
		if trimmed == scan.Text() {
			return true, nil
		}
	}

	return false, nil
}

func delLine(path, line string) error {

	fd, err := os.OpenFile(path, os.O_RDWR|os.O_SYNC, 0600)
	if err != nil {
		return err
	}
	// defer is LIFO
	defer fd.Close()
	defer fd.Sync()

	lines := []string{}
	trimmed := strings.TrimSpace(line)

	scan := bufio.NewScanner(fd)
	for scan.Scan() {

		curLine := scan.Text()

		if trimmed == curLine {
			log.Infof("deleting line: %s...", curLine[0:10])
			continue
		}

		log.Infof("appending line: %s...", curLine[0:10])
		lines = append(lines, curLine+"\n")
	}

	// rewind and write everything but the line to delete.
	err = fd.Truncate(0)
	if err != nil {
		return err
	}

	_, err = fd.Seek(0, 0)
	if err != nil {
		return err
	}

	log.Infof("%s w/out line: %+v", path, lines)

	for _, l := range lines {
		fd.Write([]byte(l))
	}

	return nil
}

// mkDir creates the whole path of dirs in `p` if it doesn't exist.
// Only if it's created, the mode/ownership is set.  Umask is ignored.
// Some bits, like os.ModeSetgid, are not set by MkdirAll, need to
// call Chmod separately, but only for the last dir in the path
func mkDir(p string, uid, gid uint32, mode os.FileMode) error {

	log.Debugf("mkdir: %s", p)

	_, err := os.Stat(p)
	if err != nil {
		if os.IsNotExist(err) {
			// Umask affects newly created files, temporarily reset it to 0
			oldmask := syscall.Umask(0)
			defer syscall.Umask(oldmask)

			if err = os.MkdirAll(p, mode); err != nil {
				return fmt.Errorf("mkdir: %w", err)
			}

			if err = os.Chown(p, int(uid), int(gid)); err != nil {
				return fmt.Errorf("chown: %w", err)
			}

			// only for the last dir, not parents
			if err = os.Chmod(p, mode); err != nil {
				return fmt.Errorf("chmod: %w", err)
			}

			log.Infof("[%s] created dir", p)
		} else {
			return fmt.Errorf("stat: %w", err)
		}
	} else {
		log.Debugf("dir already exists")
	}

	return nil
}

func rmDir(p string) error {

	// TODO: this could take a long time. Get lease on the the etcd key.
	_, err := os.Stat(p)
	if os.IsNotExist(err) {
		return nil
	}

	err = os.RemoveAll(p)
	if err != nil {
		return fmt.Errorf("[%s] rmdir: %w", p, err)
	}

	return nil
}

// writeFileOnDiff will write the data to the file only if the existing data differs
// from the given data. If the file does not exist, it is written with data and the
// permissions given are used.
func writeFileOnDiff(path string, data []byte, mode os.FileMode) error {

	// case: file does not exist, just write it.
	fi, err := os.Stat(path)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if os.IsNotExist(err) {
		// new file, just write it.
		err = os.WriteFile(path, data, mode)
		if err != nil {
			return err
		}
		log.Infof("Wrote new file: %s", path)

	} else if fi.Size() != int64(len(data)) {
		// diff size, overwrite.
		err = os.WriteFile(path, data, mode)
		if err != nil {
			return err
		}
		log.Infof("Overwrote diff length file: %s", path)

	} else {
		// file has same size as data. Do comparison.
		// write on different bytes in the file.
		eq, err := bytesToFileCmp(path, data)
		if err != nil {
			return nil
		}

		if !eq {
			log.Infof("Overwriting file with new data %s", path)
			err = os.WriteFile(path, data, mode)
			if err != nil {
				return err
			}
		} else {
			log.Debugf("Not overwriting equal file %s", path)
		}
	}

	// check and force file perms.
	fi, err = os.Stat(path)
	if err != nil {
		log.Debugf("Not changing file mods")
		return err
	}

	if fi.Mode() != mode {
		log.Debugf("Changing file mods %s: %s -> %s", path, fi.Mode(), mode)
		return os.Chmod(path, mode)
	}

	return nil
}

// bytesToFileCmp - compares the given bytes to the bytes in the given
// file. precondition: bytes and file size are the same.
func bytesToFileCmp(path string, data []byte) (bool, error) {

	const bufSize = 64000

	f, err := os.Open(path)
	if err != nil {
		return false, err
	}

	start := 0
	for {
		b1 := make([]byte, bufSize)
		readLen, err1 := f.Read(b1)
		b1 = b1[:readLen] // trim slice to length read.

		if err1 != nil {
			if err1 == io.EOF {
				return true, nil
			}
			return false, nil
		}

		end := start + bufSize
		if end > len(data) {
			end = len(data)
		}

		b2 := data[start:end]

		if !bytes.Equal(b1, b2) {
			return false, nil
		}

		start += bufSize
	}
}

// setUserGroup will set a file user and group to the given
// values if they differ. Prereq: file must exist.
func setUserGroup(path string, uid, gid int) error {

	fi, err := os.Stat(path)
	if err != nil {
		return err
	}

	// Get user/group
	stat := fi.Sys().(*syscall.Stat_t)

	log.Debugf("stat %d vs %d and %d vs %d", stat.Uid, uid, stat.Gid, gid)

	if stat.Uid != uint32(uid) || stat.Gid != uint32(gid) {
		return os.Chown(path, int(uid), int(gid))
	}

	return nil
}
