package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/pkg/storage"
)

var (
	fsRoot      = "/mergefs"
	userRoot    = fsRoot + "/user"
	projectRoot = fsRoot + "/project"
	orgRoot     = fsRoot + "/organizations"
	xdcRoot     = fsRoot + "/xdc"

	privkeyName = "merge_key"
	pubkeyName  = privkeyName + ".pub"
	certName    = privkeyName + "-cert.pub"
)

//
// Locations of things in mergefs
// user home dir: <userRoot>/<username>
// user pub keys: <userRoot>/<username>/.ssh/merge_key{,.pub}
//
// xdc host keys: <xdcRoot>/<host>/auth/merge_key{,.pub}
// xdc host cert: <xdcRoot>/<host>/auth/merge_cert
// xdc passwd: <xdcRoot>/<host>/passwd
// xdc group: <xdcRoot>/<host>/group
//

func runReconciler() {

	// Things to watch:
	// * user events
	//   * add/rm create home dirs
	// * user ssh keys
	//   * writei/delete keys to/from user home dir
	// * project events
	//   * create/destroy - create/dest project dirs
	// * rally events?
	// *

	initMergeFS()

	t := storage.ReconcilerConfigMergeFS.ToReconcilerManager(
		&UserTask{},
		&ProjectTask{},
		&OrganizationTask{},
		&CredTask{},
		&XdcTask{},
		&PubkeyTask{},
	)

	for i := range t.Reconcilers {
		t.Reconcilers[i].EnsureFrequency = ensurePeriod
	}

	t.Run()
}

func initMergeFS() {

	ds := []string{userRoot, projectRoot, xdcRoot}

	for _, d := range ds {

		_, err := os.Stat(d)
		if os.IsNotExist(err) {

			err := os.Mkdir(d, 0775)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
