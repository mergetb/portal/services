package main

import (
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

var (
	ensurePeriod time.Duration = 60 * time.Second

	// Version filled in by build system.
	Version = ""
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()

	// Read optional ensure period form environment.
	value, ok := os.LookupEnv("ENSURE_PERIOD")
	if ok {
		log.Infof("setting ensure period to %s", value)
		ep, err := strconv.Atoi(value)
		if err != nil {
			log.Fatalf("Bad value for ENSURE_PERIOD: %s. Must be a number.", value)
		}
		ensurePeriod = time.Duration(ep) * time.Second
	} else {
		log.Infof("using default ensure period: %d", ensurePeriod)
	}
}

func main() {

	log.Infof("portal version: %s", Version)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	runReconciler()
}
