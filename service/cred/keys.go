package main

import (
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// getUserSSHPubKey - returns the merge portal generated
// ssh keys for the user given in ssh-rsa format (file format)
func getUserSSHKeys(username string) (string, string, error) {

	keys := storage.NewSSHUserKeyPair(username)
	err := keys.Read()
	if err != nil {
		return "", "", err
	}

	return keys.Public, keys.Private, nil
}

func getUserSSHCert(username string) (string, error) {

	c := storage.NewSSHUserCert(username)
	err := c.Read()
	if err != nil {
		return "", err
	}

	return c.Cert, nil
}

func getHostSSHKeys(host string) (string, string, error) {

	keys := storage.NewSSHHostKeyPair(host)
	err := keys.Read()
	if err != nil {
		return "", "", err
	}

	return keys.Public, keys.Private, nil
}

func getUserHostCert(host string) (string, error) {

	c := storage.NewSSHHostCert(host)
	err := c.Read()
	if err != nil {
		return "", err
	}

	return c.Cert, nil
}
