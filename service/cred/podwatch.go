package main

import (
	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"

	"gitlab.com/mergetb/portal/services/pkg/podwatch"
)

var (
	k8c            *kubernetes.Clientset
	podManagerName = "initxdcpod"
	podVersion     = int64(1)

	workerCount = 8
)

func init() {
	podwatch.InitPodWatch()
}

func runPodWatch() {

	pw := &podwatch.PodWatcher{
		OnPodAdd:       onPodAdd,
		OnPodUpdate:    onPodUpdate,
		OnPodDelete:    onPodDelete,
		WorkerPoolSize: workerCount,
		Namespace:      xdcNs,
		// we want to generate creds before the pod even spins up
		// so we handle the very first event we see.
		Unfiltered: true,
	}

	// There is only one case we care about for XDC pod watch and credentials: a bounced
	// XDC. The initial XDC creds will be generated via standard NewXDC API calls. In the case of
	// a bounced cred reconciler, we get told about XDC keys via the reconiler package. No need to
	// do initial credentials for XDC here.
	//
	// ssh-jump pods however are are created by k8s itself (not an API call), so we handle all SSH
	// cred work here. (It may have been better to create a stand-alone reconciler for
	// ssh-jump pods, alas.)

	pw.Watch()
}

func onPodAdd(pod *v1.Pod) {

	log.Infof("XDC/jump add pod callback fired for %s", pod.Name)

	// distinguish between xdc and jumps, both are watched here.
	instance, _ := podwatch.PodType(pod)
	switch instance {
	case podwatch.XDC:
		log.Debugf("Ignoring new XDC. Etcd reconcilation will handle it.")
		break

	case podwatch.JUMP:

		log.Debugf("jump pod labels: %v", pod.Labels)

		ps := []string{
			pod.Labels["svc"],  // service name
			pod.Labels["fqdn"], // external FQDN for sshing to ths host
			pod.Name,           // may not need to be here but shuld not hurt
		}

		// generate and write keys + certs to etcd if needed.
		err := hostInit(ps, pod.Labels["svc"])
		if err != nil {
			log.Errorf("ssh jump host init: %v", err)
		}
	}
}

func onPodDelete(pod *v1.Pod) {
	// No delete action for k8s pods. ssh-jump should never be deleted and if it is
	// might as well leave creds in place.
	log.Tracef("XDC delete pod callback fired")
}

func onPodUpdate(oldPod, newPod *v1.Pod) {
	log.Tracef("Ignoring update for XDC pod: %s/%s", oldPod.Name, newPod.Name)
}
