// The stats service is responsible for collecting metrics about the usage of the Merge system.
package main

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/pkg/storage"
)

const (
	// How often to collect metrics
	collectionInterval = time.Minute * 1
)

// The list of metrics collected
var (
	numUsers = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "merge_num_users",
		Help: "The number of registered users",
	})

	numProjects = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "merge_num_projects",
		Help: "The number of registered projects",
	})

	numMzs = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "merge_num_materializations",
		Help: "The current number of materializations",
	}, []string{"project"})

	totalMzs = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "merge_total_materializations",
		Help: "The total number of materializations",
	}, []string{"project"})

	numExps = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "merge_num_experiments",
		Help: "The current number of experiments",
	}, []string{"project"})

	totalExps = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "merge_total_exp",
		Help: "The total number of experiments",
	}, []string{"project"})

	numMetalNodes = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "merge_num_metal_nodes",
		Help: "The number of allocated bare metal experiment nodes",
	}, []string{"project"})

	numVirtualNodes = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "merge_num_virtual_nodes",
		Help: "The number of allocated virtual experiment nodes",
	}, []string{"project"})

	expDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "merge_mz_duration_seconds",
		Help:    "Histogram of materialization durations",
		Buckets: prometheus.ExponentialBuckets(10.0, 5.0, 5),
	}, []string{"project"})

	expMetalNodes = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "merge_exp_metal_nodes",
		Help:    "Histogram of number of bare metal nodes in an experiment",
		Buckets: prometheus.ExponentialBuckets(10.0, 5.0, 5),
	}, []string{"project"})

	expVirtualNodes = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "merge_exp_virtual_nodes",
		Help:    "Histogram of number of virtual nodes in an experiment",
		Buckets: prometheus.ExponentialBuckets(10.0, 5.0, 5),
	}, []string{"project"})

	expLinks = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "merge_exp_links",
		Help:    "Histogram of number of links in an experiment",
		Buckets: prometheus.ExponentialBuckets(10.0, 5.0, 5),
	}, []string{"project"})
)

// Thread for collecting the metric information
func collect() {
	for {
		users()
		p := projects()
		mzs(p)
		exps(p)

		time.Sleep(collectionInterval)
	}
}

func runReconciler() {
	t := storage.ReconcilerConfigStats.ToReconcilerManager(
		&mztask{},
	)

	t.Run()
}

func init() {
	log.Info("stats service starting up")

	if err := storage.InitPortalEtcdClient(); err != nil {
		log.Fatalf("connect to etcd: %v", err)
	}

	if err := storage.InitPortalMinIOClient(); err != nil {
		log.Fatalf("connect to minio: %v", err)
	}
}

func main() {
	go collect()
	go runReconciler()

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}
