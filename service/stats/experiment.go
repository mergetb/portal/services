package main

import (
	"context"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"go.etcd.io/etcd/client/v3"
)

func exps(projs []string) {
	resp, err := storage.EtcdClient.KV.Get(context.Background(), "/experiments/", clientv3.WithPrefix())
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("etcd get")
		return
	}

	expcount := make(map[string]int)

	for _, kv := range resp.Kvs {
		s := strings.Split(string(kv.Key)[1:], "/")
		// there are other keys below /experiments/:pid/:eid
		if len(s) == 3 {
			expcount[s[1]] += 1
		}
	}

	// zero out any projects that were deleted
	for _, p := range projs {
		if _, ok := expcount[p]; !ok {
			numExps.With(prometheus.Labels{"project": p}).Set(0.0)
		}
	}

	for p, count := range expcount {
		numExps.With(prometheus.Labels{"project": p}).Set(float64(count))
	}
}
