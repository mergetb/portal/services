Data Consistency
================

In general every API operation made against the Portal that changes state can 
be broken down into changes in the following places.

- State in the Portal etcd cluster
- State in the Portal MinIO cluster
- State managed by MergeFS
- State managed by the Portal Kubernetes instance
- State managed by testbed facilities

The first two, Portal etcd and MinIO, are implemented through direct
transactions on the data store.

The remaining 3 operate off a state driver model. For each of these forms of
state, MergeFS, K8S and Facility objects, there are deamon processes that
observe a specific set of objects in the Portal etcd and drive the associated
state in their respective subsystem to what the etcd objects specify.



