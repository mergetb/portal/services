#!/usr/bin/env bash

set -e 

# get the testing framework.
git clone https://gitlab.com/mergetb/portal/test
pushd test

# Set our k8s context.
export KC_ARGS="--context=$PROFILE"

# build and install the pops container on which the tests are run
# This will not return until the pod is ready
CONTAINER=$(minikube -p $PROFILE ip):5000/pops:latest CONTAINER_K8S=localhost:5000/pops:latest make pops_ctr

# run the tests.
make 
