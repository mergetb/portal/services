package internal

import (
	"google.golang.org/grpc"
)

var (
	// GRPCMaxMessageSize is the maximum message size of a grpc message
	// the option is then passed to the grpc client connection, and
	// is set in bytes.
	GRPCMaxMessageSize = 512 * 1024 * 1024
	GRPCMaxMessage     = grpc.WithDefaultCallOptions(
		grpc.MaxCallRecvMsgSize(GRPCMaxMessageSize),
		grpc.MaxCallSendMsgSize(GRPCMaxMessageSize),
	)
	GRPCMaxServer = []grpc.ServerOption{
		grpc.MaxRecvMsgSize(GRPCMaxMessageSize),
		grpc.MaxSendMsgSize(GRPCMaxMessageSize),
	}
)
