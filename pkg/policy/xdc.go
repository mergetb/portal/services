package policy

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type XDCObject struct {
	*portal.XDCStorage
}

// UserRoles for XDCs
func (x XDCObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	result := []RoleBinding{}

	if u.Username == x.Creator {
		result = append(result, RoleBinding{XDCScope, CreatorRole})
	}

	// xxx should check all projects that we're going to mount if personal/multi-project?
	proj := x.RealizationProject
	if proj == "" {
		// legacy
		proj = x.Project
	}

	p := storage.NewProject(proj)
	err := p.Read()
	if err != nil {
		return nil, err
	}

	roles, err := ProjectObject{p.Project}.UserRoles(u)
	if err != nil {
		return nil, err
	}

	// apply any Organization roles
	if p.Project.Organization != "" {
		o := storage.NewOrganization(p.Project.Organization)
		err := o.Read()
		if err == nil {
			rs, err := OrganizationObject{Organization: o.Organization}.UserRoles(u)
			if err == nil {
				roles = append(roles, rs...)
			}
		}

		// apply any Organization roles
		if p.Project.Organization != "" {
			o := storage.NewOrganization(p.Project.Organization)
			err := o.Read()
			if err != nil {
				log.Warnf("Error getting user roles for project: %s", p.Project.Organization)
			} else {
				rs, err := OrganizationObject{Organization: o.Organization}.UserRoles(u)
				if err != nil {
					roles = append(roles, rs...)
				}
			}
		}
	}
	result = append(result, roles...)

	if u.Admin {
		result = append(result, RoleBinding{ProjectScope, CreatorRole})
	}

	return result, nil
}

func ReadXdcs(caller *identity.IdentityTraits, pid string) error {

	// Read for Xdc just maps to Project roles
	return ReadProject(caller, pid)
}

func SpawnXDC(caller *identity.IdentityTraits, rq *portal.CreateXDCRequest) error {

	u, err := id2User(caller)
	if err != nil {
		return err
	}

	if u.Admin == false {
		if rq.Image != "" || rq.Memlimit != 0 || rq.Cpulimit != 0 {
			return fmt.Errorf("non admin attributes applied by non-admin")
		}
	}

	reqs := GetPolicy().Xdc[Mode(Public)].Spawn // only public mode for xdc actions atm.

	p := storage.NewProject(rq.Project)
	err = p.Read()
	if err != nil {
		return err
	}

	return Authorize(caller, reqs, ProjectObject{p.Project})
}

func DestroyXDC(caller *identity.IdentityTraits, pid, xdc string) error {

	x := storage.NewXDC(caller.Username, xdc, pid)
	err := x.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Xdc[Mode(Public)].Destroy
	return Authorize(caller, requirements, XDCObject{x.XDCStorage})
}

func AttachXDC(caller *identity.IdentityTraits, xdc, xdc_proj, real_proj string) error {

	// check if the user is an active member
	u := storage.NewUser(caller.Username)
	if err := u.Read(); err != nil {
		return err
	}
	membr, ok := u.Projects[real_proj]
	if !ok || membr.State != portal.Member_Active {
		return fmt.Errorf("must be an active member to attach to project %s", real_proj)
	}

	x := storage.NewXDC(caller.Username, xdc, xdc_proj)
	err := x.Read()
	if err != nil {
		return err
	}

	x.RealizationProject = real_proj // used for Authorization

	requirements := GetPolicy().Xdc[Mode(Public)].Attach
	return Authorize(caller, requirements, XDCObject{x.XDCStorage})
}

func DetachXDC(caller *identity.IdentityTraits, pid, xdc string) error {

	x := storage.NewXDC(caller.Username, xdc, pid)
	err := x.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Xdc[Mode(Public)].Detach
	return Authorize(caller, requirements, XDCObject{x.XDCStorage})
}
