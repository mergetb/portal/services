package policy

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// Authorize determines whether the `requestor` is allowed to act on an `object`
// given the `policy` that governs that object. `policy` is a set of role
// bindings. If the `requestor` has any of the role bindings in the `policy`
// set, the request will be approved.
func Authorize(requestor *identity.IdentityTraits, policy []RoleBinding, object Object) error {

	u, err := id2User(requestor) // will set ops to an admin user.
	if err != nil {
		return err
	}

	roles := []RoleBinding{}

	// portal admins get creator for all roles.
	if u.Admin {
		for _, s := range Scopes {
			roles = append(roles, RoleBinding{s, CreatorRole})
		}
	} else {

		roles, err = object.UserRoles(u)
		if err != nil {
			return err
		}
	}

	fields := log.Fields{
		"requestor": requestor.Username,
		"required":  fmt.Sprintf("%+v", policy),
		"provided":  fmt.Sprintf("%+v", roles),
	}
	log.WithFields(fields).Debug("Authorize")

	return authorizePolicy(policy, roles)
}

// AuthorizePolicy determines whether the `policy` is satisfied by the provided
// `roles`. The `policy` object is a set of role bindings that implies a set of
// requirements. The `roles` object is a set of role bindings that may or may
// not intersect with the `policy` requirements. Any intersection e.g., if any
// of the bindings in `roles` satisfies any role condition in `policy`,
// authorization is granted.
func authorizePolicy(policy, roles []RoleBinding) error {

	fields := log.Fields{
		"required": fmt.Sprintf("%+v", policy),
		"provided": fmt.Sprintf("%+v", roles),
	}

	for _, required := range policy {
		if required.Scope == AnyScope && required.Role == AnyRole {
			log.WithFields(fields).Debug("authorization success (Any)")
			return nil
		}
		for _, provided := range roles {
			if Satisfies(provided, required) {
				log.WithFields(fields).Debug("authorization success")
				return nil
			}
		}
	}

	log.WithFields(fields).Debug("authorization failed")
	return me.ForbiddenError("Forbidden by policy")
}

// AuthorizeCreate handles the case of creating an object where we cannot get the
// user roles from the object as it doesn't exist yet.
func AuthorizeCreate(requestor *identity.IdentityTraits, policy []RoleBinding, scope Scope) error {

	roles := []RoleBinding{{scope, AnyRole}}

	u, err := id2User(requestor)
	if err != nil {
		return err
	}

	// do not let non-user, non-admin ids do anything.
	if u.Ver == 0 && u.Admin == false {
		return fmt.Errorf("forbidden")
	}

	iroles, err := IdentityObject{Identity: requestor}.UserRoles(u)
	if err != nil {
		return err
	}

	roles = append(roles, iroles...)

	return authorizePolicy(policy, roles)
}

// AuthorizeAny handles the case where the user may not be a portal user, yet we still want the
// object to control access to itself. In this case, we map the user to Any::Any and apply
// the object authorization.
func AuthorizeAny(policy []RoleBinding, scope Scope) error {

	roles := []RoleBinding{{scope, AnyRole}}
	return authorizePolicy(policy, roles)
}

func id2User(id *identity.IdentityTraits) (*portal.User, error) {

	// ops gets a free pass
	if id.Username == PolicyAdmin {
		return &portal.User{
			Username: id.Username,
			State:    portal.UserState_Active,
			Admin:    true,
		}, nil
	}

	// else read the user account and return if active.
	u := storage.NewUser(id.Username)

	err := u.Read()
	if err != nil {
		return nil, err
	}

	// if this is a portal user, make sure they are active.
	if u.Ver > 0 {
		if u.State != portal.UserState_Active {
			return nil, me.ForbiddenError(u.Username + " is not active")
		}
	}

	if u.Ver == 0 {
		return nil, fmt.Errorf("forbidden")
	}

	return u.User, nil
}
