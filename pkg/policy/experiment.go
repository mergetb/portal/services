package policy

import (
	log "github.com/sirupsen/logrus"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// ExperimentObject interface implementation --------------------------------
type ExperimentObject struct {
	*portal.Experiment
}

// UserRoles given an existing Experiment
func (o ExperimentObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	proj := storage.NewProject(o.Project)
	err := proj.Read()
	if err != nil {
		return nil, err
	}

	roles, err := ProjectObject{proj.Project}.UserRoles(u)
	if err != nil {
		return nil, err
	}

	if o.Creator == u.Username {
		roles = append(roles, RoleBinding{ExperimentScope, CreatorRole})
	}

	for _, m := range o.Maintainers {
		if m == u.Username {
			roles = append(roles, RoleBinding{ExperimentScope, MaintainerRole})
		}
	}

	// apply any Organization roles
	if proj.Project.Organization != "" {
		o := storage.NewOrganization(proj.Project.Organization)
		err := o.Read()
		if err == nil {
			rs, err := OrganizationObject{Organization: o.Organization}.UserRoles(u)
			if err == nil {
				roles = append(roles, rs...)
			}
		}
	}

	if u.Admin {
		roles = append(roles, RoleBinding{ExperimentScope, CreatorRole})
	}

	return roles, nil
}

// XXX
// ProjectAndExperimentRoles given the context of the project
// and experiment, return the roles for the given user
func ProjectAndExperimentRoles(u *portal.User, pid string, eid string) ([]RoleBinding, error) {

	fields := log.Fields{
		"user":       u.Username,
		"project":    pid,
		"experiment": eid,
		"roles":      nil,
	}

	// project roles wrt to user are needed for experiment policy
	p := storage.NewProject(pid)
	err := p.Read()
	if err != nil {
		return nil, err
	}
	proles, err := ProjectObject{p.Project}.UserRoles(u)
	if err != nil {
		return nil, err
	}

	fields["roles"] = proles
	log.WithFields(fields).Debug("project roles")

	e := storage.NewExperiment(eid, pid)
	err = e.Read()
	if err != nil {
		return nil, err
	}
	eroles, err := ExperimentObject{e.Experiment}.UserRoles(u)
	if err != nil {
		return nil, err
	}

	fields["roles"] = eroles
	log.WithFields(fields).Debug("experiment roles")

	return append(proles, eroles...), nil
}

// CreateExperiment ...
func CreateExperiment(caller *identity.IdentityTraits, pid, eid string) error {

	// project roles wrt to user are needed for experiment creation.
	p := storage.NewProject(pid)
	err := p.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Experiment[Mode(p.AccessMode)].Create

	return Authorize(caller, requirements, ProjectObject{p.Project})
}

// ReadExperiment ...
func ReadExperiment(caller *identity.IdentityTraits, pid, eid string) error {

	e := storage.NewExperiment(eid, pid)
	err := e.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Experiment[Mode(e.AccessMode)].Read

	return Authorize(caller, requirements, ExperimentObject{e.Experiment})
}

// UpdateExperiment ...
func UpdateExperiment(caller *identity.IdentityTraits, pid, eid string) error {

	e := storage.NewExperiment(eid, pid)
	err := e.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Experiment[Mode(e.AccessMode)].Update

	return Authorize(caller, requirements, ExperimentObject{e.Experiment})
}

// DeleteExperiment ...
func DeleteExperiment(caller *identity.IdentityTraits, pid, eid string) error {

	e := storage.NewExperiment(eid, pid)
	err := e.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Experiment[Mode(e.AccessMode)].Delete

	return Authorize(caller, requirements, ExperimentObject{e.Experiment})
}
