package merror

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"runtime"
)

// Log this error at the Error level. Returns itself.
func (me *MergeError) Log() error {

	me.logError(nil)
	return me
}

// LogWithFields logs the error and returns itself
func (me *MergeError) LogWithFields(flds log.Fields) error {

	me.logError(flds)
	return me
}

// Log log as Merge Error if possible, else just log.
func Log(e error) error {

	var me *MergeError
	if errors.As(e, &me) {
		// Jump up one level of the stack.
		me.logError(nil)
	} else {
		_, file, line, _ := runtime.Caller(1)
		log.WithFields(log.Fields{"line": line, "file": file}).Error(e)
	}
	return e
}

// LogWithFields log as Merge Error if possible, else just log.
func LogWithFields(e error, fields log.Fields) error {

	var me *MergeError
	if errors.As(e, &me) {
		// Jump up one level of the stack.
		me.logError(fields)
	} else {
		_, file, line, _ := runtime.Caller(1)
		l := log.WithFields(fields)
		l.WithFields(log.Fields{"line": line, "file": file})
		log.WithFields(fields).Error(e)
	}
	return e
}

// logError does the actual logging
func (me *MergeError) logError(fs log.Fields) {

	_, file, line, _ := runtime.Caller(2)

	estr := ""
	if me.Err != nil {
		estr = me.Err.Error()
	}

	fields := log.Fields{
		"line":      line,
		"file":      file,
		"type":      me.Type,
		"title":     me.Title,
		"detail":    me.Detail,
		"instance":  me.Instance,
		"evidence":  me.Evidence,
		"timestamp": me.Timestamp,
		"err":       estr,
	}

	l := log.WithFields(fields)

	// If we have extra fields, log them.
	// TODO: think about adding these fields to the error instance itself.
	if fs != nil {
		l = l.WithFields(fs)
	}

	l.Error("merge error")
}
