package cred

import (
	"testing"
)

func TestKeyGen(t *testing.T) {

	pub, priv, err := GenerateSSHKeyPair("murphy@mergetb.net")
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("pub key: \n%s", pub)
	t.Logf("priv key: \n%s", priv)
}
