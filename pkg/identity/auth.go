package identity

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	ory "github.com/ory/kratos-client-go"
	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	// The sole user that is always in charge of identity and is an admin.
	// The bootstrap portal identity.
	Admin string = "ops"
)

type TokenKind int

const (
	NoToken TokenKind = iota
	Cookie
	Bearer
	Basic
)

func (t TokenKind) String() string {
	switch t {
	case Cookie:
		return "cookie"
	case Bearer:
		return "bearer"
	case Basic:
		return "basic"
	}
	return "unknown"
}

func SessionFromToken(token string, tk TokenKind) (*ory.Session, error) {

	cli := KratosPublicCli()

	switch tk {
	case Bearer, Basic:
		s, _, err := cli.FrontendAPI.ToSession(context.Background()).XSessionToken(token).Execute()
		return s, err
	case Cookie:
		s, _, err := cli.FrontendAPI.ToSession(context.Background()).Cookie(token).Execute()
		return s, err
	}

	return nil, fmt.Errorf("bad token type")
}

func GRPCCaller(ctx context.Context, allow_inactive bool) (*IdentityTraits, error) {

	u, id, err := GetUserAndTraits(ctx)
	if err != nil {
		return nil, err
	}
	if allow_inactive {
		return id, nil
	}
	if u != nil && u.State != portal.UserState_Active {
		return nil, merror.ToGRPCError(merror.UserInactiveError("The Portal admin must activate this account"))
	}

	return id, nil
}

func GetUserAndTraits(ctx context.Context) (*portal.User, *IdentityTraits, error) {

	token, tokenKind, err := AccessTokenFromGrpcContext(ctx)
	if err != nil {
		return nil, nil, status.Error(codes.NotFound, err.Error())
	}

	s, err := SessionFromToken(token, tokenKind)
	if err != nil {
		log.Debugf("Error `V0alpha2Api.ToSession``: %v\n", err)
		return nil, nil, status.Error(codes.PermissionDenied, "No auth sesssion found")
	}

	// response from `ToSession`: Session
	// log.Debugf("Found Session`: %+v\n", s)

	userId, err := OrySessionToId(s)
	if err != nil {
		return nil, nil, status.Error(codes.PermissionDenied, err.Error())
	}

	log.Debugf("read traits: %+v", userId.Traits)

	if userId.Traits.Username == Admin {
		userId.Traits.Admin = true
		// Portal ops - no user data to return so just return now.
		return nil, &userId.Traits, nil
	}

	// get merge params (passed via http headers) and package merge params into Traits
	mp, err := GetMergeParamsFromGrpcContext(ctx)
	if err != nil {
		return nil, nil, status.Errorf(codes.Internal, "Error reading merge grpc params: %s", err)
	}
	for mkey, mval := range mp {
		if val, ok := userId.Traits.Traits[mkey]; ok {
			log.Errorf("Not overwriting traits %s existing value %s with merge param %s", mkey, val, mval)
			continue
		}
		userId.Traits.Traits[mkey] = mval
	}

	u := storage.NewUser(userId.Traits.Username)
	err = u.Read()
	if err != nil {
		return nil, nil, status.Errorf(codes.Internal, "Error reading user data for %s: %s", userId.Traits.Username, err)
	}

	return u.User, &userId.Traits, nil
}

func GetMergeParamsFromGrpcContext(ctx context.Context) (map[string]string, error) {

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, fmt.Errorf("missing call metadata")
	}

	mparams := make(map[string]string)
	for k, v := range md {
		// This is the cli case: find all headers starting with "x-mergetb-param",
		// chop that prefix and save the result in the map
		// Note that all headers are already lowercase
		if strings.HasPrefix(k, portal.GrpcParamHeaderPrefix) {
			mp_name := strings.TrimPrefix(k, portal.GrpcParamHeaderPrefix)
			mp_val := v[0]
			mparams[mp_name] = mp_val
		}
	}
	if gw_cookies, ok := md["grpcgateway-cookie"]; ok {
		// This is the browser (grpc gateway) case: check if passed as a cookie
		for _, values := range gw_cookies {
			if !strings.Contains(values, portal.GrpcParamHeaderPrefix) {
				continue
			}
			tokens := strings.Split(values, "; ")
			for _, token := range tokens {
				parts := strings.SplitN(token, "=", 2)
				if len(parts) == 2 {
					if strings.HasPrefix(parts[0], portal.GrpcParamHeaderPrefix) {
						mp_name := strings.TrimPrefix(parts[0], portal.GrpcParamHeaderPrefix)
						mp_val := parts[1]
						mparams[mp_name] = mp_val
					}
				}
			}
		}
	}

	return mparams, nil
}

func AccessTokenFromGrpcContext(ctx context.Context) (string, TokenKind, error) {

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", NoToken, fmt.Errorf("missing call metadata")
	}

	for k, v := range md {

		if k == "grpcgateway-cookie" {
			for _, values := range v {

				// There can be multiple cookies in this "values". Format looks to be
				// "cookie=value; cookie=value; ... "
				// We are looking for: ory_kratos_session=<cookie> case (browser clients)

				if !strings.Contains(values, "ory_kratos_session") {
					continue
				}

				tokens := strings.Split(values, "; ")
				for _, token := range tokens {
					parts := strings.SplitN(token, "=", 2)
					if len(parts) == 2 {
						if parts[0] == "ory_kratos_session" {
							return token, Cookie, nil
						}
					}
				}
			}
		}

		// Authorization: Bearer <token> case (API clients)
		if strings.ToLower(k) == "authorization" && len(v) == 1 {
			parts := strings.Fields(v[0])
			if len(parts) == 2 {
				if strings.Title(parts[0]) == "Bearer" {
					return parts[1], Bearer, nil
				}
			}
		}
	}

	return "", NoToken, fmt.Errorf("no access token provided")

}

func AccessTokenFromHTTPRequest(r *http.Request) (string, TokenKind, error) {

	header, authFound := r.Header["Authorization"]
	if authFound {
		log.Debugf("found Authorization header: %+v", header)
		parts := strings.Fields(header[0]) // Basic <token>
		if len(parts) == 2 {
			if strings.Title(parts[0]) == "Basic" {
				return parts[1], Basic, nil
			} else if strings.Title(parts[0]) == "Bearer" {
				return parts[1], Bearer, nil
			}
		}
	}

	return "", NoToken, nil // not an error.
}

func OrySessionToId(s *ory.Session) (*Identity, error) {

	if s.Active == nil {
		return nil, fmt.Errorf("User session has no active state")
	}

	if *s.Active == false {
		return nil, fmt.Errorf("User auth session not active")
	}

	var oryId *ory.Identity
	var ok bool
	if oryId, ok = s.GetIdentityOk(); !ok {
		return nil, fmt.Errorf("No identity in user auth session")
	}

	userId := NewIdentity()
	err := userId.FromOryID(oryId)
	if err != nil {
		return nil, fmt.Errorf("Unable to read user traits: %s", err)
	}

	return userId, nil
}

func GetIdForUser(username string) (*Identity, error) {

	// Ory does not give us an API to find a single ID by a trait as the trait is post run.
	// So we need to run through them all to find the one we want.

	pageSize := int64(512)
	var pageToken string
	var cnt int

	cli := KratosAdminCli()

	for cnt = 0; cnt < 10; cnt++ { // I don't trust Ory to give us the next token, so put a cap of 10 loops on this.

		ids, resp, err := cli.IdentityAPI.ListIdentities(context.Background()).PageSize(pageSize).PageToken(pageToken).Execute()
		if err != nil {
			return nil, err
		}

		for _, id := range ids {
			userId := NewIdentity()
			err = userId.FromOryID(&id)
			if err != nil {
				log.Errorf("Unable to read user traits: %s", err)
				continue
			}

			if userId.Traits.Username == username {
				return userId, nil
			}
		}

		pageToken, err = GetNextPageToken(resp)
		if err != nil {
			return nil, err
		}

		if pageToken == "" {
			break
		}

	}

	return nil, fmt.Errorf("Identity account not found for %s", username)
}
