package storage

import (
	"fmt"

	v1 "k8s.io/api/core/v1"

	"gitlab.com/mergetb/portal/services/pkg/podwatch"
)

type Pod struct {
	*v1.Pod

	name string
	Path string
	Ver  int64
}

func NewPod(path string, pod *v1.Pod) *Pod {

	return &Pod{
		Pod:  pod,
		Path: path,
	}

}

func NewPodRaw(name, path string) *Pod {
	return &Pod{
		name: name,
		Path: path,
	}
}

func (p *Pod) Bucket() string {

	if p == nil {
		return "podwatch"
	}

	return "podwatch/" + p.Path

}

func (p *Pod) Id() string {

	if p == nil {
		return ""
	}

	if p.Pod == nil {
		return p.name
	}

	switch t, _ := podwatch.PodType(p.Pod); t {
	case podwatch.XDC:
		name, ok1 := p.Labels["name"]
		proj, ok2 := p.Labels["proj"]

		if ok1 && ok2 {
			return XdcId(name, proj)
		}
	case podwatch.JUMP:
		return "ssh-jump"
	}

	return p.Pod.GetName()
}

func (p *Pod) Key() string {

	return "/" + p.Bucket() + "/" + p.Id()
}

func (p *Pod) Value() interface{} {

	return p.Pod
}

func (p *Pod) Substrate() Substrate {

	return Etcd
}

func (p *Pod) Read() error {

	if p.Name == "" {
		return fmt.Errorf("incomplete creation information")
	}

	_, err := etcdTx(readOps(p)...)
	return err
}

func (p *Pod) GetVersion() int64 {

	return p.Ver
}

func (p *Pod) SetVersion(v int64) {

	p.Ver = v
}

func (p *Pod) Create() (*Rollback, error) {

	if p.Id() == "" {
		return nil, fmt.Errorf("incomplete creation information")
	}

	r, err := etcdTx(writeOps(p)...)
	if err != nil {
		return nil, fmt.Errorf("pod write: %w", err)
	}

	return r, nil
}

func (p *Pod) Update() (*Rollback, error) {

	if p.Id() == "" {
		return nil, fmt.Errorf("incomplete creation information")
	}

	r, err := etcdTx(writeOps(p)...)
	if err != nil {
		return nil, fmt.Errorf("pod write: %w", err)
	}

	return r, nil
}

func (p *Pod) Delete() (*Rollback, error) {

	if p.Id() == "" {
		return nil, fmt.Errorf("incomplete creation information")
	}

	r, err := etcdTx(delOps(p)...)
	if err != nil {
		return nil, fmt.Errorf("pod del: %w", err)
	}

	return r, nil
}
