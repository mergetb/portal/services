package sfe

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/xir/v0.3/go"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func (rt RoutingTable) Paths(from *xir.Interface, to TPA) *Waypoint {

	log.Debugf("%s.%s ~~~> %s", from.Device.Id(), from.PortName(), to)

	wp := paths(from, to, rt, make(map[string]bool), true)

	log.Debugf("~~~")

	return wp

}

func (rt RoutingTable) AnyPaths(from *xir.Interface, to TPA) *Waypoint {

	//fmt.Printf("%s.%s ~~~> %s\n", from.Device.Id(), from.PortName(), to)

	wp := paths(from, to, rt, make(map[string]bool), false)

	//fmt.Println("~~~")

	return wp

}

func (rt RoutingTable) ShortestPath(from *xir.Interface, to TPA) (*Waypoint, portal.Diagnostics) {

	return shortestPath(from, to, rt, make(map[string]bool), true)
}

type NextHop struct {
	Interface  *xir.Interface
	Connection *xir.Connection
}

type Waypoint struct {
	In  *xir.Interface
	Out map[*NextHop]*Waypoint
}

func paths(
	in *xir.Interface,
	to TPA,
	rt RoutingTable,
	_visited map[string]bool,
	firsthop bool,
) *Waypoint {

	wp := &Waypoint{
		In:  in,
		Out: make(map[*NextHop]*Waypoint),
	}

	visited := make(map[string]bool)
	visited[in.Device.Id()] = true
	for k, v := range _visited {
		visited[k] = v
	}

	for _, route := range rt[in.Device.Id()] {

		if firsthop && route.Dev != in {
			continue
		}

		if route.Destination == to {
			//fmt.Printf("^%s:%s -> %s:%s\n", in.Device.Id(), in.PortName(), route.NextHop.Device.Id(), route.NextHop.PortName())
			nh := &NextHop{Interface: route.Dev, Connection: route.Connection}
			wp.Out[nh] = &Waypoint{
				In: route.NextHop,
			}
			break
		}

		if route.Contains(to) && !visited[route.NextHop.Device.Id()] {
			//fmt.Printf("^%s:%s -> %s:%s\n", in.Device.Id(), in.PortName(), route.NextHop.Device.Id(), route.NextHop.PortName())
			nh := &NextHop{Interface: route.Dev, Connection: route.Connection}
			wp.Out[nh] = paths(route.NextHop, to, rt, visited, false)
		}

	}

	return wp

}

func shortestPath(
	in *xir.Interface,
	to TPA,
	rt RoutingTable,
	_visited map[string]bool,
	firsthop bool,
) (*Waypoint, portal.Diagnostics) {

	var ds portal.Diagnostics

	wp := &Waypoint{
		In:  in,
		Out: make(map[*NextHop]*Waypoint),
	}

	visited := make(map[string]bool)
	visited[in.Device.Id()] = true
	for k, v := range _visited {
		visited[k] = v
	}

	// It's possible for the source and destination to be the same
	if TPA(in.Port().TPA) == to {
		return wp, ds
	}

	// The first in device is the device we need to use for nexthop
	if firsthop {
		route := rt.GetRouteUsing(in)

		if route == nil {
			diag := DiagnosticErrorf(
				"%s.%s could not be found in the routing table",
				in.Device.Id(), in.PortName(),
			)
			diag.Host = in.Device.Id()

			ds = append(ds, diag)

			return wp, ds
		}

		//fmt.Printf("^%s:%s -> %s:%s\n", in.Device.Id(), in.PortName(), route.NextHop.Device.Id(), route.NextHop.PortName())
		nh := &NextHop{Interface: route.Dev, Connection: route.Connection}
		new_wp, d := shortestPath(route.NextHop, to, rt, visited, false)

		wp.Out[nh] = new_wp
		ds = append(ds, d...)

		return wp, ds
	}

	route := rt.GetRandomShortestRouteTo(in.Device.Id(), to)

	if route == nil {
		diag := DiagnosticErrorf(
			"could not find a route from %s to %s",
			in.Device.Id(), to,
		)
		diag.Host = in.Device.Id()

		ds = append(ds, diag)

		return wp, ds
	}

	if route.Destination == to {
		//fmt.Printf("^%s:%s -> %s:%s\n", in.Device.Id(), in.PortName(), route.NextHop.Device.Id(), route.NextHop.PortName())
		nh := &NextHop{Interface: route.Dev, Connection: route.Connection}
		wp.Out[nh] = &Waypoint{
			In: route.NextHop,
		}
	} else if !visited[route.NextHop.Device.Id()] {
		//fmt.Printf("^%s:%s -> %s:%s\n", in.Device.Id(), in.PortName(), route.NextHop.Device.Id(), route.NextHop.PortName())
		nh := &NextHop{Interface: route.Dev, Connection: route.Connection}
		new_wp, d := shortestPath(route.NextHop, to, rt, visited, false)

		wp.Out[nh] = new_wp
		ds = append(ds, d...)
	}

	return wp, ds

}

func (wp *Waypoint) numStems(_visited map[string]bool, foundStems int) int {

	if wp == nil {
		return foundStems
	}

	visited := make(map[string]bool)
	visited[wp.In.Device.Id()] = true
	for k, v := range _visited {
		visited[k] = v
	}

	if wp.In.Device.Resource().HasRole(xir.Role_Stem) {
		foundStems++
	}

	for _, w := range wp.Out {
		foundStems = w.numStems(visited, foundStems)
	}

	return foundStems
}

func (wp *Waypoint) IsLocal() bool {

	return wp.numStems(make(map[string]bool), 0) < 2
}

func CollectLinks(wp *Waypoint) []*xir.Connection {

	var result []*xir.Connection

	collectLinks(wp, make(map[*xir.Connection]bool), result)

	return result

}

func collectLinks(
	wp *Waypoint,
	seen map[*xir.Connection]bool,
	result []*xir.Connection,
) {

	for nh, next := range wp.Out {
		if !seen[nh.Connection] {
			result = append(result, nh.Connection)
			seen[nh.Connection] = true
		}
		//result = append(result, collectLinks(next, seen)...)
		collectLinks(next, seen, result)
	}

}

func CollectHops(wp *Waypoint) [][]*xir.Interface {

	hs := collectHops(wp)
	return hs

}

func collectHops(wp *Waypoint) [][]*xir.Interface {

	var result [][]*xir.Interface

	//log.Printf("P==== %s:%s", wp.In.Device.Id(), wp.In.PortName())

	if len(wp.Out) > 0 {

		for nh, next := range wp.Out {
			//log.Printf("P++++ %s:%s", nh.Interface.Device.Id(), nh.Interface.PortName())

			rs := []*xir.Interface{wp.In}
			if nh.Interface != wp.In {
				rs = append(rs, nh.Interface)
			}

			hss := collectHops(next)
			if len(hss) > 0 {
				for _, hs := range hss {
					rss := append(rs, hs...)
					result = append(result, rss)
				}
			} else {
				result = append(result, rs)
			}

		}

	} else {

		return [][]*xir.Interface{{wp.In}}

	}

	return result

}
