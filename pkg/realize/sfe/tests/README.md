# Adding New Facilties
Save the facility to json
```
./facility save -j
```

# Adding New Experiments
Install mx
```
git clone git@gitlab.com:mergetb/xir.git
pip install xir/v0.3/mx
```

Save the model to json
```
python3 service/model/compileToJson.py model.py
```

# Adding/Updating Expected Results For Tests
## Template
Start with this template.
```
func TestLH_BNB(t *testing.T) {
	expected := ``

	testLH_Experiment(t, Success, expected, "experiments/bnb.json")
}
```

Note that that `expected` to start is empty.
If you're updating a test, it might already have stuff in it, which is ok.

## Run Your Test
```
$ go test ./pkg/realize/sfe -run TestLH_BNB
=== RUN   TestLH_BNB
--- FAIL: TestLH_BNB (0.07s)
    sfe_test_util.go:515: 
        `{"Nodes":{"a":"buoy000 (BareMetal)","b":"beacon000 (BareMetal)","c":"buoy001 (BareMetal)"},"Links":{"a.0~b.0":{"Segments":{"100":{"Endpoints":[{"Host":"buoy000","Ifx":"ens1f3","Net":"Physical","Ext":""},{"Host":"beacon000","Ifx":"ens1np0","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xleaf0","Ifx":"swp1s3","Net":"Access","Ext":""},{"Host":"xleaf0","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf0","Ifx":"xspine0","Net":"Peer","Ext":""},{"Host":"xspine0","Ifx":"xleaf0","Net":"Peer","Ext":""},{"Host":"xspine0","Ifx":"xleaf64","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"xspine0","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf64","Ifx":"swp4","Net":"Access","Ext":""}]}}},"b.1~c.0":{"Segments":{"101":{"Endpoints":[{"Host":"beacon000","Ifx":"ens2np0","Net":"Physical","Ext":""},{"Host":"buoy001","Ifx":"ens1f3","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xleaf0","Ifx":"swp2s3","Net":"Access","Ext":""},{"Host":"xleaf0","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf0","Ifx":"xspine1","Net":"Peer","Ext":""},{"Host":"xspine1","Ifx":"xleaf0","Net":"Peer","Ext":""},{"Host":"xspine1","Ifx":"xleaf64","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"xspine1","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf64","Ifx":"swp3","Net":"Access","Ext":""}]}}},"infranet":{"Segments":{"102":{"Endpoints":[{"Host":"buoy000","Ifx":"ens2np0","Net":"Vlan","Ext":""},{"Host":"beacon000","Ifx":"eno2","Net":"Vlan","Ext":""},{"Host":"buoy001","Ifx":"ens2np0","Net":"Vlan","Ext":""},{"Host":"ifr0","Ifx":"enp33s0f1np1","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"ileaf40g0","Ifx":"swp2s0","Net":"Trunk","Ext":""},{"Host":"ileaf40g0","Ifx":"","Net":"Vtep","Ext":""},{"Host":"ileaf40g0","Ifx":"ispine","Net":"Peer","Ext":""},{"Host":"ispine","Ifx":"ileaf40g0","Net":"Peer","Ext":""},{"Host":"ispine","Ifx":"swp1","Net":"Peer","Ext":""},{"Host":"ifr0","Ifx":"enp33s0f1np1","Net":"Peer","Ext":""},{"Host":"ileaf1g","Ifx":"swp1","Net":"Trunk","Ext":""},{"Host":"ileaf1g","Ifx":"ispine","Net":"Trunk","Ext":""},{"Host":"ispine","Ifx":"ileaf1g","Net":"Trunk","Ext":""},{"Host":"ispine","Ifx":"","Net":"Vtep","Ext":""},{"Host":"ileaf40g0","Ifx":"swp2s1","Net":"Trunk","Ext":""}]}}}}}`
    sfe_test_util.go:521: 
        expected:
        NODES (0)
        LINKS (0)
        
        
    sfe_test_util.go:524: 
        bnb.json:
        NODES (3)
        a -> buoy000 (BareMetal)
        b -> beacon000 (BareMetal)
        c -> buoy001 (BareMetal)
        LINKS (3)
        a.0~b.0
          ENDPOINTS:
            [100] Physical @ buoy000.ens1f3
            [100] Physical @ beacon000.ens1np0
          WAYPOINTS:
            [100] Access @ xleaf0.swp1s3
            [100] Vtep @ xleaf0
            [100] Peer @ xleaf0.xspine0
            [100] Peer @ xspine0.xleaf0
            [100] Peer @ xspine0.xleaf64
            [100] Peer @ xleaf64.xspine0
            [100] Vtep @ xleaf64
            [100] Access @ xleaf64.swp4
        b.1~c.0
          ENDPOINTS:
            [101] Physical @ beacon000.ens2np0
            [101] Physical @ buoy001.ens1f3
          WAYPOINTS:
            [101] Access @ xleaf0.swp2s3
            [101] Vtep @ xleaf0
            [101] Peer @ xleaf0.xspine1
            [101] Peer @ xspine1.xleaf0
            [101] Peer @ xspine1.xleaf64
            [101] Peer @ xleaf64.xspine1
            [101] Vtep @ xleaf64
            [101] Access @ xleaf64.swp3
        infranet
          ENDPOINTS:
            [102] Vlan @ buoy000.ens2np0
            [102] Vlan @ beacon000.eno2
            [102] Vlan @ buoy001.ens2np0
            [102] Vtep @ ifr0.enp33s0f1np1
          WAYPOINTS:
            [102] Trunk @ ileaf40g0.swp2s0
            [102] Vtep @ ileaf40g0
            [102] Peer @ ileaf40g0.ispine
            [102] Peer @ ispine.ileaf40g0
            [102] Peer @ ispine.swp1
            [102] Peer @ ifr0.enp33s0f1np1
            [102] Trunk @ ileaf1g.swp1
            [102] Trunk @ ileaf1g.ispine
            [102] Trunk @ ispine.ileaf1g
            [102] Vtep @ ispine
            [102] Trunk @ ileaf40g0.swp2s1
        
        
    sfe_test_util.go:482: 
        message:"unmarshal mini realization: unexpected end of JSON input"  level:Error
        message:"expected, bnb.json: embeddings differ"  level:Error
        
    sfe_test_util.go:488: diagnostics had an unexpected error

FAIL
exit status 1
FAIL    gitlab.com/mergetb/portal/services/pkg/realize/sfe      0.058s
```

If you're satisfied with the generated output on `sfe_test_util.go:524` 
and how it compares to the expected output on `sfe_test_util.go:521`, 
you can go ahead and copy the generated output as a golang JSON string on `sfe_test_util.go:515` into expected, like so:

```
func TestLH_BNB(t *testing.T) {
	expected := `{"Nodes":{"a":"buoy000 (BareMetal)","b":"beacon000 (BareMetal)","c":"buoy001 (BareMetal)"},"Links":{"a.0~b.0":{"Segments":{"100":{"Endpoints":[{"Host":"buoy000","Ifx":"ens1f3","Net":"Physical","Ext":""},{"Host":"beacon000","Ifx":"ens1np0","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xleaf0","Ifx":"swp1s3","Net":"Access","Ext":""},{"Host":"xleaf0","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf0","Ifx":"xspine0","Net":"Peer","Ext":""},{"Host":"xspine0","Ifx":"xleaf0","Net":"Peer","Ext":""},{"Host":"xspine0","Ifx":"xleaf64","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"xspine0","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf64","Ifx":"swp4","Net":"Access","Ext":""}]}}},"b.1~c.0":{"Segments":{"101":{"Endpoints":[{"Host":"beacon000","Ifx":"ens2np0","Net":"Physical","Ext":""},{"Host":"buoy001","Ifx":"ens1f3","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xleaf0","Ifx":"swp2s3","Net":"Access","Ext":""},{"Host":"xleaf0","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf0","Ifx":"xspine1","Net":"Peer","Ext":""},{"Host":"xspine1","Ifx":"xleaf0","Net":"Peer","Ext":""},{"Host":"xspine1","Ifx":"xleaf64","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"xspine1","Net":"Peer","Ext":""},{"Host":"xleaf64","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xleaf64","Ifx":"swp3","Net":"Access","Ext":""}]}}},"infranet":{"Segments":{"102":{"Endpoints":[{"Host":"buoy000","Ifx":"ens2np0","Net":"Vlan","Ext":""},{"Host":"beacon000","Ifx":"eno2","Net":"Vlan","Ext":""},{"Host":"buoy001","Ifx":"ens2np0","Net":"Vlan","Ext":""},{"Host":"ifr0","Ifx":"enp33s0f1np1","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"ileaf40g0","Ifx":"swp2s0","Net":"Trunk","Ext":""},{"Host":"ileaf40g0","Ifx":"","Net":"Vtep","Ext":""},{"Host":"ileaf40g0","Ifx":"ispine","Net":"Peer","Ext":""},{"Host":"ispine","Ifx":"ileaf40g0","Net":"Peer","Ext":""},{"Host":"ispine","Ifx":"swp1","Net":"Peer","Ext":""},{"Host":"ifr0","Ifx":"enp33s0f1np1","Net":"Peer","Ext":""},{"Host":"ileaf1g","Ifx":"swp1","Net":"Trunk","Ext":""},{"Host":"ileaf1g","Ifx":"ispine","Net":"Trunk","Ext":""},{"Host":"ispine","Ifx":"ileaf1g","Net":"Trunk","Ext":""},{"Host":"ispine","Ifx":"","Net":"Vtep","Ext":""},{"Host":"ileaf40g0","Ifx":"swp2s1","Net":"Trunk","Ext":""}]}}}}}`

	testLH_Experiment(t, Success, expected, "experiments/bnb.json")
}
```

Rerun the test, and verify that it passes.
```
$ go test ./pkg/realize/sfe -run TestLH_BNB
PASS
ok      gitlab.com/mergetb/portal/services/pkg/realize/sfe      0.060s
```

# Testing Notes
We compare and store testing specific data structures similar to the actual data structures in the api or xir.
We do not compare or store actual data structures in the api or xir.


The reason for this is because storing the actual data structures can break when the api or xir updates,
but these testing specific structures will not break on api or xir updates.


This means that there is a conversion function between the api/xir data structures to the testing specific data structures,
which might break or have to be updated when the api or xir gets updated, but in this case, we will have a compiler error,
which is a lot more informative than the test failing because of api/xir mismatches.

The exception to this are the experiment and facility models, as they are pregenerated instead of generated when needed.
To combat this, they are stored as jsons, which will hopefully be more clear when it fails, and are a bit more resiliant to updates.
